﻿$(document).ready(function () {
    var user_role = sessionStorage.getItem("user_type");
    if (user_role == "Customer") {
        $('#new-seller-button').hide();
        CustomerSegment();
    }
    else if (user_role == "Seller") {
        $('#new-seller-button').hide();
        SellerSegment();
    }
    else {
        $('#new-seller-button').show();
        AdminSegment();

        $("#new-seller-button").click(function () { $("#modalContainer").css("display", "block"); } );

        $("#close-new-seller").click(function (){
            $("#modalContainer").css("display", "none"); 
        }
          
        );

    }

    $('#new-product-button').click(function () {
        $('#product-name').val("");
        $('#product-title').text("");
        $('#product-price').val("");
        $('#product-quantity').val("");
        $('#product-description').val("");
        $('#product-city').val("");
        $('#productModalAdd').css('display', 'block');

    });

});

function AddNewSeller() {
    var username = $("#txt-usernames").val();
    var pass = $("#txt-passwords").val();
    var passconf = $("#txt-confirm-passwords").val();
    var fname = $("#txt-fnames").val();
    var lname = $("#txt-lnames").val();
    var email = $("#txt-emails").val();
    var date = $("#txt-dates").val();
    var gender = $("#genders").val();

    var seller = {
        "Username": username,
        "Password": pass,
        "FirstName": fname,
        "Lastname": lname,
        "Email": email,
        "DateOfBirth": date,
        "Gender": gender
    };


    $.ajax({
        url: "/api/registration/Seller",
        type: "POST",
        data: seller,
       
        success: function () {
            alert("Uspesno dodat nov prodavac");
        },
        error: function (error) {
            alert(error.responseJSON["Message"]);
        }
    });

    $("#modalContainer").css("display", "none");


}
var orderStatus = ["Active", "Executed", "Canceled"];

function AdminSegment() {
    $('.customer-section').css("display", "none");
    $('#seller-section').css("display", "none");
    $('#admin-section').css("display", "flex");
    $(document).on('click', '#add-new-seller', AddNewSeller);


    $.ajax({
        url: '/api/admin/GetNonAdminUsers',
        type: 'GET',
        success: function (data) {
            var users = data;
            populateUsersTable(users);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('Greška prilikom dobavljanja korisnika:', errorThrown);
        }
    });

    $.ajax({
        type: "GET",
        url: "/api/order/ForAdmin",
        success: function (data) {
            FillOrdersForAdmin(data);
        },
        error: function () {
            alert("Failed to retrieve orders for admin.");
        }
    });

    $.ajax({
        url: '/api/review/ReturnAll',
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            if (response && response.length > 0) {
                populateReviewTable(response);
            }
        },
        error: function (xhr, status, error) {
            console.log('Error: ' + error);
        }
    });
    LoadProductsForAdmin();

}

function LoadProductsForAdmin() {
    $("#product-image-upload").on("change", function (event) {
        var selectedFile = event.target.files[0];


        var reader = new FileReader();
        reader.onload = function (event) {
            $("#product-image").attr("src", event.target.result);
            currentImage = event.target.result;
        };
        reader.readAsDataURL(selectedFile);
    });
    $("#product-image-upload1").on("change", function (event) {
        var selectedFile = event.target.files[0];

        var reader = new FileReader();
        reader.onload = function (event) {
            $("#product-image1").attr("src", event.target.result);
            currentImage = event.target.result;
        };
        reader.readAsDataURL(selectedFile);
    });

    $.ajax({
        url: "/api/product",
        type: "GET",
        success: function (data) {
            LoadProducts(data, "admin-products-div");
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function populateReviewTable(reviews) {
    var tableBody = $('#admin-review-table tbody');
    tableBody.empty();

    $.each(reviews, function (index, review) {
        var row = $('<tr>');
        row.append('<td>' + review.Product.Name + '</td>');
        row.append('<td>' + review.Reviewer.FirstName + ' ' + review.Reviewer.LastName + '</td>');
        row.append('<td>' + review.Title + '</td>');
        row.append('<td>' + review.Content + '</td>');
        row.append('<td>' + (review.Approved ? 'Da' : 'Ne') + '</td>');
        row.append('<td><button class="ApproveReview" data-reviewid="' + review.Id + '">Odobri</button></td>');
        row.append('<td><button class="DeclineReview" data-reviewid="' + review.Id + '">Odbij</button></td>');
        tableBody.append(row);
    });
    ;


    $(document).on('click', '.ApproveReview', function () {
        var reviewId = $(this).data('reviewid');

        $.ajax({
            type: "GET",
            url: '/api/review/Approve/' + reviewId,
            success: function () {
                alert("Recenzija je prihvacena");
                location.reload();
            },
            error: function (data) {
                alert(data.responseJSON["Message"]);
            }
        });
    });

    $(document).on('click', '.DeclineReview', function () {
        var reviewId = $(this).data('reviewid');

        $.ajax({
            type: "GET",
            url: '/api/review/Decline/' + reviewId,
            success: function () {
                alert("Recenzija je odbijena");
                location.reload();
            },
            error: function (data) {
                alert(data.responseJSON["Message"]);
            }
        });
    });

}

function FillOrdersForAdmin(data) {
    var tableBody = $("#admin-order-table tbody");
    tableBody.empty();

    $.each(data, function (index, order) {
        var row = $("<tr></tr>");
        d = new Date(order.OrderDate);
        date = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
        row.append("<td>" + order.Id + "</td>");
        row.append("<td>" + order.Product.Name + "</td>");
        row.append("<td>" + order.Quantity + "</td>");
        row.append("<td>" + date + "</td>");
        row.append("<td>" + orderStatus[order.Status] + "</td>");

        if (orderStatus[order.Status] === "Active") {
            row.append('<td><button class="MarkOrderDelivered" data-orderid="' + order.Id + '">Označi prispeće</button></td>');
            row.append('<td><button class="CancelOrder" data-orderid="' + order.Id + '">Otkazi prispece</button></td>');
        }


        tableBody.append(row);
    });

    $(document).on('click', '.MarkOrderDelivered', function () {
        var orderId = $(this).data('orderid');

        $.ajax({
            type: "GET",
            url: '/api/order/CompleteOrder/' + orderId,
            success: function () {
                alert("Porudžbina je označena kao izvršena");
                location.reload();
            },
            error: function (data) {
                alert(data.responseJSON["Message"]);
            }
        });
    });

    $(document).on('click', '.CancelOrder', function () {
        var orderId = $(this).data('orderid');

        $.ajax({
            type: "GET",
            url: '/api/order/DeleteOrder/' + orderId,
            success: function () {
                alert("Porudžbina je označena kao otkazana");
                location.reload();
            },
            error: function (data) {
                alert(data.responseJSON["Message"]);
            }
        });
    });
}

var currentImage = null;

function SellerSegment() {
    $('.customer-section').css("display", "none");
    $('#seller-section').css("display", "flex");
    $('#admin-section').css("display", "none");
    $("#product-image-upload").on("change", function (event) {
        var selectedFile = event.target.files[0];


        var reader = new FileReader();
        reader.onload = function (event) {
            $("#product-image").attr("src", event.target.result);
            currentImage = event.target.result;
        };
        reader.readAsDataURL(selectedFile);
    });
    $("#product-image-upload1").on("change", function (event) {
        var selectedFile = event.target.files[0];

        var reader = new FileReader();
        reader.onload = function (event) {
            $("#product-image1").attr("src", event.target.result);
        };
        reader.readAsDataURL(selectedFile);
    });

    $.ajax({
        url: "/api/product/Seller",
        type: "GET",
        success: function (data) {
            LoadProducts(data, "seller-products-div");
        },
        error: function (error) {
            console.log(error);
        }
    });



    //https://stackoverflow.com/questions/14267781/sorting-html-table-with-javascript taken from this site
    var getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;
    var comparer = function (idx, asc) {
        return function (a, b) {
            return function (v1, v2) {
                return (v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2))
                    ? v1 - v2
                    : v1.toString().localeCompare(v2);
            }(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
        }
    };

    $(document).on('click', '#products-div th', function () {
        const th = $(this)[0];
        const table = th.closest('table');
        Array.from(table.querySelectorAll('tr:nth-child(n+2)'))
            .sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc))
            .forEach(tr => table.appendChild(tr));
    });

    $('#search-button').click(function () {
 
        var name = $('#search-name').val().toLocaleLowerCase();
        var city = $('#search-city').val().toLocaleLowerCase();

        var min = $('#search-min-price').val();
        if (min == "") { min = 0; }
        var max = $('#search-max-price').val();
        if (max == "") { max = 100000; }
     
        table = $('#products-table tr:not(:first)').filter(function () {
  
            $(this).toggle(
                ($(this.children[2]).text().toLowerCase().indexOf(name) > -1) &&
                ($(this.children[6]).text().toLowerCase().indexOf(city) > -1) &&
                (parseInt($(this.children[3]).text()) >= min) &&
                (parseInt($(this.children[3]).text()) <= max)
            )
        });
    });
}

function LoadProducts(products, divname) {
    
    $('#new-product-button1').click(function () {
        $('#product-name1').val("");
        $('#product-title1').text("");
        $('#product-price1').val("");
        $('#product-quantity1').val("");
        $('#product-description1').val("");
        $('#product-city1').val("");
        $('#productModalAdd').css('display', 'block');

    });


    $(document).on('click', '#submitChangesAdd', function () {
        var imageElement = $('#product-image1');
        var imageFile = $('#product-image-upload1').prop('files')[0];
        var reader = new FileReader();
        reader.onloadend = function () {
            var imageData = reader.result;
            imageElement.attr('src', imageData);

            var base64Data = imageData.replace(/^data:image\/(png|jpg|jpeg);base64,/, '');
            convertedImage = atob(base64Data);
        };

        if (imageFile) {
            reader.readAsDataURL(imageFile);
        }
        var productName = $('#product-title1').val();
        var productPrice = $('#product-price1').val();
        var productQuantity = $('#product-quantity1').val();
        var productCity = $('#product-city1').val();
        var productDescription = $('#product-description1').val();
        var product = {
            Name: productName,
            Price: productPrice,
            City: productCity,
            Quantity: productQuantity,
            Description: productDescription,
            Image: currentImage
        };

        $.ajax({
            type: 'PUT',
            url: '/api/product/AddProduct',
            data: JSON.stringify(product),
            contentType: 'application/json',
            success: function () {
                alert('Proizvod je uspešno dodat.');
            },
            error: function (error) {
                alert('Došlo je do greške pri ažuriranju proizvoda.' + error);
            }
        });

        $('#productModalAdd').css('display', 'none');
    });
    if (products.length == 0) {
        $('#' + divname + '').html("Trenutno nema proizvoda");
    } else {
        let productsTable = '<table id="products-table" border="1px solid">';
        var user_role = sessionStorage.getItem("user_type");

        if (user_role == "Customer") {
            productsTable += '<thead><tr><th>Slika</th><th>Id</th><th>Naziv</th><th>Cena</th><th>Količina</th><th>Opis</th><th>Grad</th><th colspan="1"></th></tr></thead>';
        } else if (user_role == "Seller" || user_role == "Administrator") {
            productsTable += '<thead><tr><th>Slika</th><th>Id</th><th>Naziv</th><th>Cena</th><th>Količina</th><th>Opis</th><th>Grad</th><th colspan="1"></th></tr></thead>';
        } else {
            productsTable += '<thead><tr><th>Slika</th><th>Id</th><th>Naziv</th><th>Cena</th><th>Količina</th><th>Opis</th><th>Grad</th></tr></thead>';
        }

        productsTable += '<tbody>';

        for (product in products) {
            let temp = products[product];
            productsTable += '<tr>';
            productsTable += '<td><img src="' + temp.Image + '"/></td><td>' + temp.Id + '</td> <td>' + temp.Name + '</td> <td>' + temp.Price + '</td> <td>' + temp.Quantity + '</td> <td>' + temp.Description + '</td> <td>' + temp.City + '</td>';

            if (user_role == "Customer") {
                productsTable += '<td><button class="AddToFavorites" id="' + temp.Id + '">Dodaj</button></td>';
            }

            if (user_role == "Customer" && temp.Quantity > 0) {
                productsTable += '<td><button class="OrderProduct" min="1" max="' + temp.Quantity + '" id="' + temp.Id + '"">Poruči</button></td>';
            }
            if (user_role != "Customer" && !temp.Deleted) {
                productsTable += '<td><button class="UpdateProduct" id="' + temp.Id + '"">Izmeni</button>';
                productsTable += '<button class="DeleteProduct" id="' + temp.Id + '"">Obrisi</button></td>';
            }

            productsTable += '</tr>';
        }

        productsTable += '</tbody></table>';
        $('#' + divname + '').html(productsTable);

        $(document).on('click', '.UpdateProduct', function () {
            id = $(this).attr('id');
            $.ajax({
                type: "GET",
                url: '/api/product/GetProductDetails/' + id,
                success: function (product) {
                    sessionStorage.setItem("tempProductId", product.Id);
                    $('#product-title').val(product.Name);
                    $('#product-image').attr('src', product.Image);
                    $('#product-price').val(product.Price);
                    $('#product-quantity').val(product.Quantity);
                    $('#product-city').val(product.City);
                    $('#product-description').val(product.Description);
                    $('#productModal').css('display', 'block');
                }
            });
        });


        $(document).on('click', '#submitChanges', function () {
            var imageElement = $('#product-image');
            var imageFile = $('#product-image-upload').prop('files')[0];
            var reader = new FileReader();
            var convertedImage;
            reader.onloadend = function () {
                var imageData = reader.result;
                imageElement.attr('src', imageData);

                var base64Data = imageData.replace(/^data:image\/(png|jpg|jpeg);base64,/, '');
                convertedImage = atob(base64Data);
            };

            if (imageFile) {
                reader.readAsDataURL(imageFile);
            }
            var productName = $('#product-title').val();
            var productPrice = $('#product-price').val();
            var productQuantity = $('#product-quantity').val();
            var productCity = $('#product-city').val();
            var productDescription = $('#product-description').val();
            var product = {
                Id: sessionStorage.getItem("tempProductId"),
                Name: productName,
                Price: productPrice,
                City: productCity,
                Quantity: productQuantity,
                Description: productDescription,
                Image: currentImage
            };

            $.ajax({
                type: 'PUT',
                url: '/api/product/UpdateProduct',
                data: JSON.stringify(product),
                contentType: 'application/json',
                success: function () {
                    alert('Proizvod je uspešno ažuriran.');
                },
                error: function (error) {
                    alert('Došlo je do greške pri ažuriranju proizvoda.' + error);
                }
            });

            $('#productModal').css('display', 'none');
        });

        $(document).on('click', '.DeleteProduct', function () {
            id = $(this).attr('id');
            $.ajax({
                type: "GET",
                url: '/api/product/DeleteProduct/' + id,
                success: function () {
                    alert("Uspesno obrisano.");
                    window.location.href = 'Index.html';
                },
                error: function (data) {
                    alert(data.responseJSON["Message"]);
                }
            });
        });
        
        $(document).on('click', '#close-update-product', function () {
            $('#productModal').css('display', 'none');
        });


        productModalAdd

        $(document).on('click', '#close-add-product', function () {
            $('#productModalAdd').css('display', 'none');
        });
    }
}

function CustomerSegment() {
    $('.customer-section').css("display", "flex");
    $('#seller-section').css("display", "none");
    $('#admin-section').css("display", "none");
    loadOrders();

    $('#toggle-orders').click(function (event) {
        event.preventDefault();
        if ($('#orders-section').css("display") == "flex") {
            $('#orders-section').css("display", "none");
        }
        else {
            $('#orders-section').css("display", "flex");
        }
    });

    $.ajax({
        type: "GET",
        url: "/api/product/getFavorite",
        success: function (data) {
            populateProductsTable(data);
        },
        error: function () {
            alert("Failed to retrieve favorite products.");
        }
    });

    $('#toggle-products').click(function (event) {
        event.preventDefault();
        if ($('#fav-products-section').css("display") == "flex") {
            $('#fav-products-section').css("display", "none");
        }
        else {
            $('#fav-products-section').css("display", "flex");
        }
    });

    $('#add-new-review').click(function (event) {
        event.preventDefault();

        var orderId = localStorage.getItem("currentOrderId");
        var comment = $('#review-title').val();
        var description = $('#review-description').val();

        var review = {
            "Id": orderId,
            "Title": comment,
            "Content": description
        };

        $.ajax({
            type: "POST",
            url: '/api/review/AddReview',
            data: review,
            success: function () {
                alert("Recenzija je uspešno dodata");
                location.reload();
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.responseText;
                alert("Greška prilikom dodavanja recenzije: " + errorMessage);
            }
        });


        $('#reviewModal').css('display', 'none');
    });
}

function loadOrders() {
    $.ajax({
        type: "GET",
        url: "/api/order/ForCurrentUser",
        success: function (data) {
            populateOrdersTable(data);
        },
        error: function () {
            alert("Failed to retrieve orders.");
        }
    });
}

function populateOrdersTable(data) {
    var tableBody = $("#order-table tbody");
    tableBody.empty();

    $.each(data, function (index, order) {
        var row = $("<tr></tr>");
        d = new Date(order.OrderDate);
        date = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
        row.append("<td>" + order.Id + "</td>");
        row.append("<td>" + order.Product.Name + "</td>");
        row.append("<td>" + order.Quantity + "</td>");
        row.append("<td>" + date + "</td>");
        row.append("<td>" + orderStatus[order.Status] + "</td>");
        if (orderStatus[order.Status] === "Active") {
            row.append('<td><button class="MarkOrderDelivered" data-orderid="' + order.Id + '">Označi prispeće</button></td>');
        }
        if (orderStatus[order.Status] === "Executed") {
            row.append('<td><button class="LeaveReview" data-orderid="' + order.Id + '">Ostavi recenziju</button></td>');
            row.append('<td>' +
                '<button class="DeleteReview" data-reviewid="' + order.Id + '">Obriši recenziju</button>' +
                '</td>');
        }

        tableBody.append(row);
    });

    $(document).on('click', '.MarkOrderDelivered', function () {
        var orderId = $(this).data('orderid');

        $.ajax({
            type: "GET",
            url: '/api/order/CompleteOrder/' + orderId,
            success: function () {
                alert("Porudžbina je označena kao izvršena");
                loadOrders();
            },
            error: function (data) {
                alert(data.responseJSON["Message"]);
            }
        });
    });

    $(document).on('click', '.LeaveReview', function () {
        var orderId = $(this).data('orderid');
        localStorage.setItem("currentOrderId", orderId);
        $('#reviewModal').css('display', 'block');
    });

    $('.close').click(function () {
        $('#reviewModal').css('display', 'none');
    });

    $(window).click(function (event) {
        if (event.target.id === 'reviewModal') {
            $('#reviewModal').css('display', 'none');
        }
    });

    $(document).on('click', '.DeleteReview', function () {
        var reviewId = $(this).data('reviewid');

        $.ajax({
            type: "POST",
            url: '/api/review/DeleteReview/' + reviewId,
            success: function () {
                alert("Recenzija je uspešno obrisana");
                location.reload();
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.responseText;
                alert("Greška prilikom brisanja recenzije: " + errorMessage);
            }
        });
    });
}

function populateProductsTable(data) {
    var tableBody = $("#product-table tbody");
    tableBody.empty();

    $.each(data, function (index, product) {
        var row = $("<tr></tr>");
        row.append("<td>" + product.Id + "</td>");
        row.append("<td>" + product.Name + "</td>");
        row.append("<td>" + product.Price + "</td>");
        row.append("<td>" + product.Quantity + "</td>");
        row.append("<td>" + product.Description + "</td>");
        row.append("<td>" + product.City + "</td>");

        tableBody.append(row);
    });
}

function populateUsersTable(users) {
    //https://stackoverflow.com/questions/14267781/sorting-html-table-with-javascript taken from this site
    var getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;
    var comparer = function (idx, asc) {
        return function (a, b) {
            return function (v1, v2) {
                return (v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2))
                    ? v1 - v2
                    : v1.toString().localeCompare(v2);
            }(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));
        }
    };

    $(document).on('click', '#users-table th', function () {
        const th = $(this)[0];
        const table = th.closest('table');
        Array.from(table.querySelectorAll('tr:nth-child(n+2)'))
            .sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc))
            .forEach(tr => table.appendChild(tr));
    });

    var roles = ["Kupac", "Prodavac", "Administrator"]
    var tableBody = $('#users-table tbody');
    tableBody.empty();

    for (var i = 0; i < users.length; i++) {
        var user = users[i];
        var row = $('<tr></tr>');
        row.append($('<td></td>').text(user.Id));
        row.append($('<td></td>').text(user.FirstName));
        row.append($('<td></td>').text(user.LastName));
        row.append($('<td></td>').text(user.Username));
        row.append($('<td></td>').text(roles[user.Role]));
        row.append('<td>' +
            '<button class="DeleteUser" data-userid="' + user.Id + '">Obriši</button>' +
            '</td>');
        tableBody.append(row);
    }
    $(document).on('click', '.DeleteUser', function () {
        var userid = $(this).data('userid');

        $.ajax({
            type: "GET",
            url: '/api/registration/Delete/' + userid,
            success: function () {
                alert("Korisnik uspesno obrisan.");
                location.reload();
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.responseText;
                alert("Greška: " + errorMessage);
            }
        });
    });

    $('#search-btnq').click(function () {
        var name = $('#search-nameq').val().toLocaleLowerCase().trim();
        var lastname = $('#search-lastnameq').val().toLocaleLowerCase().trim();
        var rolesrc = $('#search-roleq').val().toLocaleLowerCase();
        table = $('#users-table tr:not(:first)').filter(function () {
            $(this).toggle(
                ($(this.children[1]).text().toLowerCase().indexOf(name) > -1) &&
                ($(this.children[2]).text().toLowerCase().indexOf(lastname) > -1) &&
                ($(this.children[4]).text().toLowerCase().indexOf(rolesrc) > -1)

            )
        });
    });

}